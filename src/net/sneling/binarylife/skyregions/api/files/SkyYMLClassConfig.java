package net.sneling.binarylife.skyregions.api.files;

import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;
import net.sneling.binarylife.skyregions.api.util.logger.Logger;

import java.io.File;
import java.lang.reflect.Field;

/**
 * Created by Sneling on 27/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyYMLClassConfig extends SkyYMLConfig {

    /**
     * Create a YML config.
     *
     * @param plugin
     * @param file
     */
    public SkyYMLClassConfig(SkyPlugin plugin, File file) {
        super(plugin, file, true);
    }

    @Override
    public void loadResource(){
        try {
            for (Field f : getClass().getDeclaredFields()) {
                if (!getConfig().isSet(toID(f.getName())))
                    getConfig().set(toID(f.getName()), f.get(getClass())); // TODO check for wrappers
                else
                    f.set(getClass(), getConfig().get(toID(f.getName())));
            }
        }catch (IllegalAccessException e){
            Logger.error("An error occured while loading the config.yml: ");
            e.printStackTrace();
        }
    }

    public String toID(String in){
        return in.toLowerCase().replaceAll("_", ".");
    }

}