package net.sneling.binarylife.skyregions.api.files;

import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyYMLConfig extends SkyFile {

    private FileConfiguration config;
    private final boolean loadFromResource;

    /**
     * Create a YML config.
     * @param plugin
     * @param file
     * @param loadFromResource
     */
    public SkyYMLConfig(SkyPlugin plugin, File file, boolean loadFromResource) {
        super(plugin, file);

        this.loadFromResource = loadFromResource;

        load();

        if(this.loadFromResource) {
            try {
                loadResource();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void loadResource() throws Exception{
        // TODO: 24/11/2016 LOAD FROM THE RES FOLDER
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void load(){
        config = YamlConfiguration.loadConfiguration(getFile());
    }

    public void save() throws IOException {
        config.save(getFile());
    }

    public boolean isLoadFromResource() {
        return loadFromResource;
    }

}