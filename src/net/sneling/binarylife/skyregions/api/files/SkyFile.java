package net.sneling.binarylife.skyregions.api.files;

import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyFile {

    private final SkyPlugin plugin;
    private final File file;

    /**
     * Create a file that is attached to a plugin.
     * Creation of the file is fully handled.
     * @param plugin
     * @param file
     */
    public SkyFile(SkyPlugin plugin, File file) {
        this.plugin = plugin;
        this.file = file;

        if(!new File(file.getParent()).exists())
            new File(file.getParent()).mkdirs();

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public SkyPlugin getPlugin() {
        return plugin;
    }

    public File getFile() {
        return file;
    }

}