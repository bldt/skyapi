package net.sneling.binarylife.skyregions.api.util.logger;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Logger {

    private static final String PREFIX = LColor.GREEN + "[SkyAPI]" + LColor.RESET;
    private static final String ERR = LColor.RED + "[ERROR]";
    private static final String DEBUG = LColor.CYAN + "[DEBUG]" + LColor.RESET;

    public static void log(Object obj){
        print(PREFIX + obj);
    }

    public static void debug(Object obj){
        print(PREFIX + DEBUG + obj);
    }

    public static void error(Object obj){
        print(PREFIX + ERR + obj + LColor.RESET);
    }

    public static void print(Object object){
        System.out.println(object);
    }

}