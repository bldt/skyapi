package net.sneling.binarylife.skyregions.api.util;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class StringUtil {

    public static String capitalizeFirst(String in){
        if(in.length() == 0)
            return "";

        return in.substring(0, 1).toUpperCase() + in.substring(1);
    }


}