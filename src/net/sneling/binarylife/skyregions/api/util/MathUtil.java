package net.sneling.binarylife.skyregions.api.util;

import java.math.BigDecimal;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class MathUtil {

    public static boolean isInteger(String s){
        try{
            Integer.parseInt(s);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public static int parseInt(String s, int d){
        try{
            return Integer.parseInt(s);
        }catch (NumberFormatException e){
            return d;
        }
    }

    public static boolean isDouble(String s){
        try{
            Double.parseDouble(s);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public static String formatDecimals(double d, int places){
        BigDecimal bd = new BigDecimal(d);

        bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
        return String.valueOf(bd.doubleValue());
    }

//    public static String formatFigures(double d, int figures){
//        BigDecimal bd = new BigDecimal(d);
//
//        bd = bd.setScale(figures, BigDecimal.ROUND_HALF_UP);
//        return String.valueOf(bd.doubleValue());
//    }


}