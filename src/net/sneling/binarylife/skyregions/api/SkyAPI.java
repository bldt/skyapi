package net.sneling.binarylife.skyregions.api;

import net.sneling.binarylife.skyregions.api.a.SkyAPILang;
import net.sneling.binarylife.skyregions.api.a.SkyAPIPerm;
import net.sneling.binarylife.skyregions.api.commands.CommandManager;
import net.sneling.binarylife.skyregions.api.permissions.SkyPermission;
import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyAPI extends SkyPlugin {

    private static SkyAPI instance;

    private final CommandManager commandManager;

    public SkyAPI() {
        instance = this;
        this.commandManager = new CommandManager();
    }

    public void onLoad(){
        try {
            new SkyAPILang();

            SkyPermission.load(SkyAPIPerm.class, "skyapi");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void onEnable(){
    }

    public void onDisable(){
    }

    public static SkyAPI getInstance() {
        return instance;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

}