package net.sneling.binarylife.skyregions.api.a;

import net.sneling.binarylife.skyregions.api.permissions.SkyPermission;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyAPIPerm {

    public static SkyPermission
    API_ALL,
    API_SAVECONFIGS;

}