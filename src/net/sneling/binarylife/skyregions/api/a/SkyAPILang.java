package net.sneling.binarylife.skyregions.api.a;

import net.sneling.binarylife.skyregions.api.SkyAPI;
import net.sneling.binarylife.skyregions.api.lang.SkyLanguage;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyAPILang extends SkyLanguage {

    public SkyAPILang() {
        super(SkyAPI.getInstance(), "lang.yml");
    }
    
    public static String
    HOLD,

    COMMAND_HELP_NO_PERM = "&cYou are not allowed to view the help for this command!",
    COMMAND_HELP_CMD = "&a%NAME% - %DESC%",
    COMMAND_HELP_HEADER = "&a[Help] -- %COMMAND% -- %INFO%",
    COMMAND_HELP_NO_PAGE = "&cYou went too far! (No more help found)",
    COMMAND_HELP_NO_CONTENT = "&cThere is no help for this command",
    COMMAND_INVALID_SENDER = "&cYou must be a %TYPE% to execute this command!",
    COMMAND_INVALID_ARGUMENT = "&cArgument %ARG% was invalid. Please use a(n)%TYPE%!",
    COMMAND_MISSING_ARGUMENT = "&cYou forgot some mendatory arguments for this command!",
    COMMAND_NO_PERMISSION = "&cYou are not allowed to use this command"
    ;

}