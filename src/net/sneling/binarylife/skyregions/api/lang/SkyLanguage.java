package net.sneling.binarylife.skyregions.api.lang;

import net.md_5.bungee.api.ChatColor;
import net.sneling.binarylife.skyregions.api.files.SkyYMLConfig;
import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyLanguage extends SkyYMLConfig {

    /**
     * Create a Language file
     * @param plugin The plugin in which this will be stored
     * @param filename The name of the file (do not include .yml)
     */
    public SkyLanguage(SkyPlugin plugin, String filename) {
        super(plugin, new File(plugin.getDataFolder() + "/" + filename), true);
    }

    @Override
    public void loadResource() throws IllegalAccessException, IOException {
        for(Field f: getClass().getDeclaredFields()){
            if(!f.getType().equals(String.class))
                 continue;

            if(!getConfig().isSet(toID(f.getName()))) {
                getConfig().set(toID(f.getName()), f.get(getClass()));
                f.set(getClass(), ChatColor.translateAlternateColorCodes('&', String.valueOf(f.get(getClass()))));
            } else {
                f.set(getClass(), ChatColor.translateAlternateColorCodes('&', getConfig().getString(toID(f.getName()))));
            }
        }

        save();
    }

    @SuppressWarnings("WeakerAccess")
    public static String toID(String s){
        return s.toLowerCase().replaceAll(" ", "-   ").replaceAll("_", ".");
    }

}