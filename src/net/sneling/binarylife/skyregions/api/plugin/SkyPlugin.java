package net.sneling.binarylife.skyregions.api.plugin;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyPlugin extends JavaPlugin {

    public abstract void onLoad();

    public abstract void onEnable();

    public abstract void onDisable();


}