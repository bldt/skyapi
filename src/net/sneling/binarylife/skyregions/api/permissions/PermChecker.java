package net.sneling.binarylife.skyregions.api.permissions;

import org.bukkit.permissions.Permissible;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class PermChecker {

    public static boolean has(Permissible p, SkyPermission permission){
        return permission == null || has(p, permission.getID());
    }

    public static boolean has(Permissible p, String s){
        return p.hasPermission(s);
    }

}