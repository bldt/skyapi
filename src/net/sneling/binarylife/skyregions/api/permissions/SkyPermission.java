package net.sneling.binarylife.skyregions.api.permissions;

import net.sneling.binarylife.skyregions.api.util.logger.Logger;
import org.bukkit.permissions.Permissible;

import java.lang.reflect.Field;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class SkyPermission {

    private String id, description;

    public SkyPermission() {
        this("No Description Set");
    }

    public SkyPermission(String description) {
        this.description = description;
    }

    SkyPermission setID(String id){
        this.id = id;
        return this;
    }

    /**
     * Gets the permission ID for this. (This is loaded when load(Class, String) is called)
     * @return The Permission ID
     */
    public String getID(){
        return id;
    }

    public boolean has(Permissible p){
        return PermChecker.has(p, this);
    }

    /**
     * Load the permissions of the class.
     * This needs to be called in the constructor of your plugin, as well as for each permission class.
     * Also, you must define a prefix for all your permissions.
     * For example, with prefix 'skyrg', the field COMMAND_BUY would become skyrh.command.buy
     * @param c The class that contains the permissions
     * @param prefix The prefix for your permission
     * @throws IllegalAccessException
     */
    public static void load(Class c, String prefix) throws IllegalAccessException {
        for(Field f: c.getDeclaredFields()){
            if(!f.getType().equals(SkyPermission.class))
                continue;

            SkyPermission perm = (SkyPermission) f.get(c);

            if(perm == null)
                perm = new SkyPermission();

            perm.setID(prefix + "." + toID(f.getName()));

            f.set(c, perm);
        }
    }

    private static String toID(String s){
        return s.toLowerCase().replaceAll("_", ".");
    }

    public String getDescription() {
        return description;
    }
}