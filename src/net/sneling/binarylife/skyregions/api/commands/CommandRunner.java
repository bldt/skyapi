package net.sneling.binarylife.skyregions.api.commands;

import net.sneling.binarylife.skyregions.api.SkyAPI;
import net.sneling.binarylife.skyregions.api.a.SkyAPILang;
import net.sneling.binarylife.skyregions.api.permissions.PermChecker;
import net.sneling.binarylife.skyregions.api.util.MathUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
class CommandRunner implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        List<SkyCommand> cmds = SkyAPI.getInstance().getCommandManager().getCommands();

        for(SkyCommand cmd: cmds){
            if(cmd.isValidCall(command.getName())){
                List<String> args = new ArrayList<>();

                Collections.addAll(args, strings);

                findAndExecuteCommand(commandSender, cmd, args);

                return true;
            }
        }

        return false;
    }

    private void findAndExecuteCommand(CommandSender sender, SkyCommand base, List<String> args){
        if(!PermChecker.has(sender, base.getPermission())){ // TODO: 24/11/2016 NOTE: THIS IS ALSO CHECKED IN THE RUN FUNCTION. REMOVE?
            sender.sendMessage(SkyAPILang.COMMAND_NO_PERMISSION);
            return;
        }

        if(args.size() == 0){
            if(!base.run(sender, args)) // Help will be sent (helpful if the person doesn't input the correct arguments, for example)
                HelpGenerator.sendHelp(sender, base, 1);
        }else{
            SkyCommand potentialChild = getMatchingChild(base, args.get(0));

            if(potentialChild == null){
                if(args.get(0).equalsIgnoreCase("help") || args.get(0).equalsIgnoreCase("?")){ // handle help
                    int page;

                    if(args.size() >= 2){
                        page = MathUtil.parseInt(args.get(1), 1);
                    }else{
                        page = 1;
                    }

                    HelpGenerator.sendHelp(sender, base, page);
                }else if(!base.run(sender, args)){
                    HelpGenerator.sendHelp(sender, base, 1);
                }
            }else{
                System.out.println(args.get(0));
                args.remove(0);
                findAndExecuteCommand(sender, potentialChild, args);
            }
        }
    }

    private SkyCommand getMatchingChild(SkyCommand cmd, String name){
        if(cmd.getChilds() == null || cmd.getChilds().isEmpty())
            return null;

        for(SkyCommand child: cmd.getChilds())
            if(child.isValidCall(name))
                return child;

        return null;
    }

}