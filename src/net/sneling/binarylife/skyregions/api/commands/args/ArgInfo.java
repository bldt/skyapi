package net.sneling.binarylife.skyregions.api.commands.args;

import net.sneling.binarylife.skyregions.api.util.MathUtil;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class ArgInfo {

    private final String name;
    private String def;
    private final ArgType type;
    private final List<ArgRequirement> requirements;

    /**
     * Information for an argument
     * @param name The name of the argument
     * @param type The type (If the sender has to use the argument or no)
     * @param requirements The type of data that you are expecting for this argument
     */
    public ArgInfo(String name, ArgType type, ArgRequirement... requirements) {
        this.name = name;
        this.type = type;

        this.requirements = new ArrayList<>();

        Collections.addAll(this.requirements, requirements);
    }

    public enum ArgType {
        MENDATORY,
        OPTIONAL
    }

    /**
     * For optional arguments, if no value is entered, what you entered will be used.
     * @param def The default value that will be used
     */
    public final ArgInfo setDefault(String def) {
        this.def = def;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getDefault() {
        return def;
    }

    public ArgType getType() {
        return type;
    }

    public List<ArgRequirement> getRequirements() {
        return requirements;
    }

    /**
     * Checks if the value entered for the argument is valid
     * @param s The value inputed by the sender
     * @return Whether the value was conform to the requirements or not.
     */
    @SuppressWarnings("deprecation")
	public boolean validate(String s){
        for(ArgRequirement req: requirements){
            switch (req){
                case PLAYER:
                    if(Bukkit.getPlayer(s) == null)
                        return false;
                    break;
                case INTEGER:
                    if (!MathUtil.isInteger(s))
                        return false;
                    break;
                case DOUBLE:
                    if(!MathUtil.isDouble(s))
                        return false;
                    break;
                case OFFLINE_PLAYER:
                    if(Bukkit.getOfflinePlayer(s) == null)
                        return false;
                    break;
            }
        }

        return true;
    }

    public String format(){
        if(type == ArgType.MENDATORY){
            return "<" + name + formatRequirements() + ">";
        }else{
            return "[" + name + formatRequirements() + "]";
        }
    }

    public String formatRequirements(){
        if(requirements == null || requirements.isEmpty())
            return "";

        String res = " (";

        for(ArgRequirement req: requirements){
            res += " & " + req.getDisplayName();
        }

        return res.replaceFirst(" & ", "") + ")";
    }

}