package net.sneling.binarylife.skyregions.api.commands.args;

import net.sneling.binarylife.skyregions.api.util.StringUtil;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public enum ArgRequirement {
    PLAYER,
    INTEGER,
    DOUBLE,
    OFFLINE_PLAYER;

    public String getDisplayName(){
        return StringUtil.capitalizeFirst(this.toString().toLowerCase());
    }

}