package net.sneling.binarylife.skyregions.api.commands;

import net.sneling.binarylife.skyregions.api.plugin.SkyPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class CommandManager {

    private final CommandRunner runner;

    private List<SkyCommand> commands = new ArrayList<>();

    public CommandManager() {
        this.runner = new CommandRunner();
    }

    public RegisterResponse registerCommand(SkyCommand command, SkyPlugin plugin){
        if(get(command.getName()) != null)
            return RegisterResponse.FAIL_EXISTS;

        commands.add(command);
        plugin.getCommand(command.getName()).setExecutor(runner);

        return RegisterResponse.SUCCESS;
    }

    public SkyCommand get(String s){
        for(SkyCommand cmd: commands)
            if (cmd.getName().equalsIgnoreCase(s))
                return cmd;

        return null;
    }

    public List<SkyCommand> getCommands() {
        return commands;
    }

    public enum RegisterResponse {
        SUCCESS,
        FAIL_EXISTS
    }

}