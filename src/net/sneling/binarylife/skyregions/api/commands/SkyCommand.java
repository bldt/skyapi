package net.sneling.binarylife.skyregions.api.commands;

import net.sneling.binarylife.skyregions.api.a.SkyAPILang;
import net.sneling.binarylife.skyregions.api.a.SkyAPIPerm;
import net.sneling.binarylife.skyregions.api.commands.args.ArgInfo;
import net.sneling.binarylife.skyregions.api.permissions.PermChecker;
import net.sneling.binarylife.skyregions.api.permissions.SkyPermission;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public abstract class SkyCommand {

    private String name = "";
    private String description = "No Description Set";
    private final List<String> aliases = new ArrayList<>();
    private final List<SkyCommand> childs = new ArrayList<>();
    private final HashMap<Integer, ArgInfo> arguments = new HashMap<>();
    private SkyPermission permission = SkyAPIPerm.API_ALL;
    private Class senderType = CommandSender.class;

    /**
     * Constructor for a command
     * @param name the name of the command (by which players can call the command)
     */
    public SkyCommand(String name) {
        this.name = name;
    }

    final boolean run(CommandSender sender, List<String> args){
        if(!PermChecker.has(sender, getPermission())){
            sender.sendMessage(SkyAPILang.COMMAND_NO_PERMISSION);
            return false;
        }

        if(!(senderType.isInstance(sender))){
            sender.sendMessage(SkyAPILang.COMMAND_INVALID_SENDER.replaceAll("%TYPE%", senderType.getName()));
            return false;
        }

        for(int i: getArguments().keySet()){
            ArgInfo argInfo = getArguments().get(i);

            if(args.size() <= i){
                if(argInfo.getType() == ArgInfo.ArgType.OPTIONAL){
                    args.add(null);
                    args.set(i, argInfo.getDefault());
                }else{
                    sender.sendMessage(SkyAPILang.COMMAND_MISSING_ARGUMENT);
                    return false;
                }
            }

            if(!argInfo.validate(args.get(i))){
                sender.sendMessage(SkyAPILang.COMMAND_INVALID_ARGUMENT.replaceAll("%ARG%", argInfo.getName()).replaceAll("%TYPE%", argInfo.formatRequirements()));
                return false;
            }
        }

        execute(sender, args);
        return true;
    }

    /**
     * Override this method to make this command do something when called (and not one of its childs)
     * @param sender The CommandSender that sent the command
     * @param args The arguments that were sent to the command. The arguments are relative to the command.
     */
    protected void execute(CommandSender sender, List<String> args){
        HelpGenerator.sendHelp(sender, this, 1);
    }

    public final String getName() {
        return name;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(String description) {
        this.description = description;
    }

    public final SkyPermission getPermission() {
        return permission;
    }

    public final void setPermission(SkyPermission permission) {
        this.permission = permission;
    }

    public final Class getSenderType() {
        return senderType;
    }

    /**
     * This will force the sender to be an instance of the Class you set
     * @param senderType The type of sender you are expecting
     */
    public final void setSenderType(Class senderType) {
        this.senderType = senderType;
    }

    /**
     * This will allow command senders to use those instead of the actual name of the command.
     * Note that this doesn't work for a 'Main' (wihtout parent) command. For this, you should define the aliases in the plugin.yml as well.
     * @param a Array of Strings for the aliases
     */
    public final void addAlias(String... a){
        for(String b: a)
            aliases.add(b.toLowerCase());
    }

    public final List<String> getAliases() {
        return aliases;
    }

    /**
     * Add childs to this command.
     * Childs are like argument, but with their own hand
     * @param a
     */
    public final void addChilds(SkyCommand... a){
        Collections.addAll(childs, a);
    }

    public final List<SkyCommand> getChilds() {
        return childs;
    }

    public final int addArgument(ArgInfo a){
        int b = getNextArgId();
        this.arguments.put(b, a);
        return b;
    }

    private int getNextArgId(){
        return arguments.size();
    }

    public final HashMap<Integer, ArgInfo> getArguments() {
        return arguments;
    }

    public final boolean isVisibleFor(CommandSender sender){
        return !(senderType.isInstance(sender) || PermChecker.has(sender, getPermission()));
    }

    public boolean isValidCall(String a){
        return name.equalsIgnoreCase(a) || aliases.contains(a.toLowerCase());
    }

    protected enum HelpType {
        EMPTY,
        DESCRIPTION,
        ALL;
    }

    public final HelpType getHelpType(){
        if(description.equals("No Description Set") && getArguments().isEmpty())
            return HelpType.EMPTY;

        if(!description.equals("No Description Set") && getArguments().isEmpty())
            return HelpType.DESCRIPTION;

        return HelpType.ALL;
    }

    public String getUsage(){
        String res = getName();

        for(int i = 0; i < getArguments().values().size(); i++)
            res += " " + getArguments().get(i).format();

        return res;
    }

}