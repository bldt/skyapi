package net.sneling.binarylife.skyregions.api.commands;

import net.sneling.binarylife.skyregions.api.a.SkyAPILang;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sneling on 23/11/2016 for SkyAPI.
 * <p>
 * Copyright &copy; 2016 - Sneling
 * <p>
 * You are not allowed to copy/use any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class HelpGenerator {

    private static final int COMMANDS_PER_PAGE = 4;

    public static void sendHelp(CommandSender sender, SkyCommand cmd, int page){
        sender.sendMessage(generateHelp(sender, cmd, page));
    }

    private static String generateHelp(CommandSender sender, SkyCommand cmd, int page){
        if(!cmd.getPermission().has(sender))
            return SkyAPILang.COMMAND_HELP_NO_PERM;

        List<SkyCommand> childs = getChildsForPage(sender, cmd, page);

        if(childs == null){ // No Childs are found
            if(cmd.getHelpType() == SkyCommand.HelpType.EMPTY){
                return SkyAPILang.COMMAND_HELP_NO_CONTENT;
            }else if(cmd.getHelpType() == SkyCommand.HelpType.DESCRIPTION){
                return generateHeader(cmd, "Info") + "\n" + generateInfo(cmd);
            }else{
                return generateHeader(cmd, "Info") + "\n" + generateInfo(cmd);
            }
        }

        if(childs.size() == 0) // No more pages
            return SkyAPILang.COMMAND_HELP_NO_PAGE;

        int nbPage;

        if(getAvailableChilds(sender, cmd).size() % COMMANDS_PER_PAGE == 0)
            nbPage = getAvailableChilds(sender, cmd).size() / COMMANDS_PER_PAGE;
        else
            nbPage = getAvailableChilds(sender, cmd).size() / COMMANDS_PER_PAGE + 1;

        String info = "";

        if(page == 1)
            info = generateInfo(cmd) + "\n";

        return generateHeader(cmd, page + "/" + nbPage) + "\n" + info + generateHelpList(childs);
    }

    private static String generateHeader(SkyCommand cmd, String info){
        return "\n"
                + SkyAPILang.COMMAND_HELP_HEADER
                .replaceAll("%COMMAND%", cmd.getName())
                .replaceAll("%INFO%", info);
    }

    private static String generateInfo(SkyCommand cmd){
        if(cmd.getHelpType() == SkyCommand.HelpType.EMPTY)
            return "";

        if(cmd.getHelpType() == SkyCommand.HelpType.DESCRIPTION)
            return ChatColor.WHITE + " " + cmd.getDescription();

        return ChatColor.GRAY + " Usage: " + cmd.getUsage() + "\n"
                + ChatColor.WHITE + cmd.getDescription();
    }

    private static List<SkyCommand> getAvailableChilds(CommandSender sender, SkyCommand cmd){
        List<SkyCommand> cmds = new ArrayList<>();

        if(cmd.getChilds() == null || cmd.getChilds().isEmpty())
            return null;

        for(SkyCommand child: cmd.getChilds())
            if(child.isVisibleFor(sender))
                cmds.add(child);

        return cmds;
    }

    private static List<SkyCommand> getChildsForPage(CommandSender sender, SkyCommand cmd, int page){
        List<SkyCommand> cmds = getAvailableChilds(sender, cmd);

        if(cmds == null || cmds.isEmpty())
            return null;

        List<SkyCommand> cmdsPage = new ArrayList<>();

        page--;
        int skip = page * COMMANDS_PER_PAGE;

        for(int i = 0; i < COMMANDS_PER_PAGE; i++) {
            try {
                cmdsPage.add(cmds.get(skip + i));
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }

        return cmdsPage;
    }

    private static String generateHelpList(List<SkyCommand> cmds){
        String rs = "";

        for(SkyCommand cmd: cmds)
            rs += generateHelpLine(cmd) + "\n";

        return rs;
    }

    private static String generateHelpLine(SkyCommand cmd){
        return SkyAPILang.COMMAND_HELP_CMD.replaceAll("%NAME%", cmd.getName()).replaceAll("%DESC%", cmd.getDescription());
    }

}